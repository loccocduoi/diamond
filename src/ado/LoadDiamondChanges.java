package ado;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.apache.commons.io.FileUtils;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class LoadDiamondChanges {
	static Map<String, Integer> fullColIndex, updateColIndex;
	static String DELI = ";";
	static String updateHeader = "IDEX_Online_Item_ID;Status;Supplier_Stock_Reference;Cut;Carat;Color;"
			+ "Natural_Fancy_Color;Natural_Fancy_Color_Intensity;Natural_Fancy_Color_Overtone;Treated_Color;Clarity;"
			+ "Make_Cut_Grade;Grading_Lab;Certificate_Number;Certificate_Path;Image_Path;"
			+ "Online_Report;Asking_Price_Per_Carat;Total_Price;Polish;Symmetry;"
			+ "Measurements;Total_Depth;Table_Width;Crown_Height;Pavilion_Depth;"
			+ "Girdle_From_To;Culet_Size;Culet_Condition;Graining;Fluorescence_Intensity;"
			+ "Fluorescence_Color;Enhancement;Supplier;Country;State_Region;"
			+ "Remarks;Phone;Matching_Pair_Stock_Reference;Email;Reference_to_IDEX_Price_Report;Date;HCA;HCA_Remark\n";
	static void init(){
		updateColIndex = new HashMap<String, Integer>();
		updateColIndex.put("id", 0);
		updateColIndex.put("status", 1);
		updateColIndex.put("sr", 2);
		updateColIndex.put("cut", 3);
		updateColIndex.put("ct", 4);
		updateColIndex.put("col", 5);
		updateColIndex.put("nfc", 6);
		updateColIndex.put("nfci", 7);
		updateColIndex.put("nfco", 8);
		updateColIndex.put("tc", 9);
		updateColIndex.put("cl", 10);
		updateColIndex.put("mk", 11);
		updateColIndex.put("lab", 12);
		updateColIndex.put("cn", 13);
		updateColIndex.put("cp", 14);
		updateColIndex.put("ip", 15);
		updateColIndex.put("or", 16);
		updateColIndex.put("ap", 17);
		updateColIndex.put("tp", 18);
		updateColIndex.put("pol", 19);
		updateColIndex.put("sym", 20);
		updateColIndex.put("mes", 21);
		updateColIndex.put("dp", 22);
		updateColIndex.put("tb", 23);
		updateColIndex.put("cr", 24);
		updateColIndex.put("pv", 25);
		updateColIndex.put("gd", 26);
		updateColIndex.put("cs", 27);
		updateColIndex.put("cc", 28);
		updateColIndex.put("gr", 29);
		updateColIndex.put("fl", 30);
		updateColIndex.put("fc", 31);
		updateColIndex.put("en", 32);
		updateColIndex.put("sup", 33);
		updateColIndex.put("cty", 34);
		updateColIndex.put("st", 35);
		updateColIndex.put("rm", 36);
		updateColIndex.put("tel", 37);
		updateColIndex.put("psr", 38);
		updateColIndex.put("eml", 39);
		updateColIndex.put("idxl", 40);
	}
	
	public static void main(String args[]) throws IOException, ParserConfigurationException, SAXException {
		init();
		Date time = Calendar.getInstance().getTime();
		final String date = new SimpleDateFormat("dd/MM/YYYY").format(time);
		
		File updateFile = new File("update" + ".csv");
		
		final FileWriter w0 = new FileWriter(updateFile);
		
		w0.write(updateHeader);
		
		SAXParserFactory factory = SAXParserFactory.newInstance();
		SAXParser saxParser = factory.newSAXParser();

		DefaultHandler updateHandler = new DefaultHandler(){
			int count = 0;
			public void startElement(String uri, String localName, String qName, Attributes attributes)
				throws SAXException{
				if (!qName.equals("item"))
					return;
				System.out.println(count++);
				String[] line = new String[41];
				for (int i = 0; i < attributes.getLength(); i++) {
					line[updateColIndex.get(attributes.getQName(i))] = attributes.getValue(i).replace(";", ",");
				}
				String textLine ="";
				for (int i = 0; i < 41; i++) {
					textLine += line[i] + DELI;
				}
				try{
						w0.write(textLine.replace("null", "") + date + ";;\n");
				}catch (Exception e){
//					e.printStackTrace();
				}
			}
		};

		System.out.println("Processing updates...");
		File inputFile = new File("update.zip");
		String updateSource = "http://idexonline.com/Idex_Feed_API-Inventory_Update?String_Access=UVO4XXEMUCJLCRP7KF4JL9E12"; 
		FileUtils.copyURLToFile(new URL(updateSource), inputFile);
		ZipInputStream zis = new ZipInputStream(new FileInputStream(inputFile));
		ZipEntry ze = zis.getNextEntry();
		if (ze==null){
			System.out.println("No new updates!");
		}else{
			String fileName = ze.getName();
			File newFile = new File(fileName);
			FileOutputStream fos = new FileOutputStream(newFile);
			byte[] buffer = new byte[1024];
			int len;
	        while ((len = zis.read(buffer)) > 0) {
	        	fos.write(buffer, 0, len);
	        }
	        fos.close();
	        zis.closeEntry();
	    	zis.close();
	    	
			saxParser.parse(newFile, updateHandler);
		}
		w0.close();

		System.out.println("Finished");
	}
}
