package ado;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.apache.commons.io.FileUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class HCA {
	static Map<String, Integer> fullColIndex;
	static Map<String, String[]> filterIndex;
	static String DELI = ";";
	static String fullHeader = "IDEX_Online_Item_ID;Supplier_Stock_Reference;Cut;Carat;Color;"
			+ "Natural_Fancy_Color;Natural_Fancy_Color_Intensity;Natural_Fancy_Color_Overtone;Treated_Color;Clarity;"
			+ "Make_Cut_Grade;Grading_Lab;Certificate_Number;Certificate_Path;Image_Path;"
			+ "Online_Report;Asking_Price_Per_Carat;Total_Price;Polish;Symmetry;"
			+ "Measurements;Total_Depth;Table_Width;Crown_Height;Pavilion_Depth;"
			+ "Girdle_From_To;Culet_Size;Culet_Condition;Graining;Fluorescence_Intensity;"
			+ "Fluorescence_Color;Enhancement;Supplier;Country;State_Region;"
			+ "Remarks;Phone;Matching_Pair_Stock_Reference;Email;Reference_to_IDEX_Price_Report;Date;HCA;HCA_Remark\n";
	static String[] agents = new String[]{
		"Mozilla/5.0 (compatible; 008/0.83; http://www.80legs.com/webcrawler.html) Gecko/2008032620",
		"ABACHOBot",
		"Accoona-AI-Agent/1.1.2 (aicrawler at accoonabot dot com)",
		"Bimbot/1.0",
		"Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36",
		"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2227.1 Safari/537.36",
		"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2227.0 Safari/537.36",
		"Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2227.0 Safari/537.36",
		"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2062.124 Safari/537.36",
		"Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2049.0 Safari/537.36",
		"Mozilla/5.0 (Windows NT 6.1; WOW64; Trident/7.0; AS; rv:11.0) like Gecko",
		"Mozilla/5.0 (compatible, MSIE 11, Windows NT 6.3; Trident/7.0; rv:11.0) like Gecko",
		"Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.1; WOW64; Trident/6.0)",
		"Mozilla/5.0 (Windows; U; MSIE 9.0; WIndows NT 9.0; en-US))",
		"LexxeBot/1.0 (lexxebot@lexxe.com)",
		"Mozilla/5.0 (BlackBerry; U; BlackBerry 9900; en) AppleWebKit/534.11+ (KHTML, like Gecko) Version/7.1.0.346 Mobile Safari/534.11+",
		"Mozilla/5.0 (BlackBerry; U; BlackBerry 9850; en) AppleWebKit/534.11+ (KHTML, like Gecko) Version/7.0.0.254 Mobile Safari/534.11+",
		"Mozilla/5.0 (BlackBerry; U; BlackBerry 9800; en-US) AppleWebKit/534.8+ (KHTML, like Gecko) Version/6.0.0.446 Mobile Safari/534.8+",
		"Opera/12.02 (Android 4.1; Linux; Opera Mobi/ADR-1111101157; U; en-US) Presto/2.9.201 Version/12.02",
		"Opera/9.80 (Android 2.3.3; Linux; Opera Mobi/ADR-1111101157; U; es-ES) Presto/2.9.201 Version/11.50",
		"Opera/9.80 (S60; SymbOS; Opera Mobi/SYB-1107071606; U; en) Presto/2.8.149 Version/11.10",
		"Opera/9.80 (Android; Linux; Opera Mobi/ADR-1012221546; U; pl) Presto/2.7.60 Version/10.5",
		"Opera/9.80 (S60; SymbOS; Opera Mobi/1209; U; sk) Presto/2.5.28 Version/10.1",
		"Opera/9.80 (S60; SymbOS; Opera Mobi/1181; U; en-GB) Presto/2.5.28 Version/10.1",
		"Opera/9.80 (Android; Linux; Opera Mobi/ADR-1011151731; U; de) Presto/2.5.28 Version/10.1"
	};
	
	static void init() throws IOException{
		fullColIndex = new HashMap<String, Integer>();
		filterIndex = new HashMap<String, String[]>();
		
		fullColIndex.put("id", 0);
		fullColIndex.put("sr", 1);
		fullColIndex.put("cut", 2);
		fullColIndex.put("ct", 3);
		fullColIndex.put("col", 4);
		fullColIndex.put("nfc", 5);
		fullColIndex.put("nfci", 6);
		fullColIndex.put("nfco", 7);
		fullColIndex.put("tc", 8);
		fullColIndex.put("cl", 9);
		fullColIndex.put("mk", 10);
		fullColIndex.put("lab", 11);
		fullColIndex.put("cn", 12);
		fullColIndex.put("cp", 13);
		fullColIndex.put("ip", 14);
		fullColIndex.put("or", 15);
		fullColIndex.put("ap", 16);
		fullColIndex.put("tp", 17);
		fullColIndex.put("pol", 18);
		fullColIndex.put("sym", 19);
		fullColIndex.put("mes", 20);
		fullColIndex.put("dp", 21);
		fullColIndex.put("tb", 22);
		fullColIndex.put("cr", 23);
		fullColIndex.put("pv", 24);
		fullColIndex.put("gd", 25);
		fullColIndex.put("cs", 26);
		fullColIndex.put("cc", 27);
		fullColIndex.put("gr", 28);
		fullColIndex.put("fl", 29);
		fullColIndex.put("fc", 30);
		fullColIndex.put("en", 31);
		fullColIndex.put("sup", 32);
		fullColIndex.put("cty", 33);
		fullColIndex.put("st", 34);
		fullColIndex.put("rm", 35);
		fullColIndex.put("tel", 36);
		fullColIndex.put("psr", 37);
		fullColIndex.put("eml", 38);
		fullColIndex.put("idxl", 39);
		
		String filename = "filter.txt";
		int day = Calendar.getInstance().get(Calendar.DAY_OF_WEEK);
		switch (day) {
		case Calendar.MONDAY:{
			filename += "monday.txt";
			break;
		}
		case Calendar.TUESDAY:{
			filename += "tuesday.txt";
			break;
		}
		case Calendar.WEDNESDAY:{
			filename += "wednesday.txt";
			break;
		}
		case Calendar.THURSDAY:{
			filename += "thursday.txt";
			break;
		}
		case Calendar.FRIDAY:{
			filename += "friday.txt";
			break;
		}
		case Calendar.SATURDAY:{
			filename += "saturday.txt";
			break;
		}
		case Calendar.SUNDAY:{
			filename += "sunday.txt";
			break;
		}
		default:
			break;
		}
		System.out.println(filename);
		BufferedReader r = new BufferedReader(new FileReader(filename));
		for(String line; (line = r.readLine()) != null;){
			line = line.toLowerCase();
			String[] options = null;
			if (!line.split(":",-1)[1].isEmpty()){
				if (line.split(":",-1)[1].contains(";"))
					options = line.split(":",-1)[1].split(";");
				else
					options = new String[]{line.split(":",-1)[1]};
			}
			filterIndex.put(line.split(":")[0], options);
		}
		r.close();
		
	}
	
	public static void main(String args[]) throws IOException, ParserConfigurationException, SAXException {
		init();
		Date time = Calendar.getInstance().getTime();
		final String date = new SimpleDateFormat("dd/MM/YYYY").format(time);
		
		final String urlGIA = "http://www.gia.edu/otmm_wcs_int/proxy-report/?url=https://myapps.gia.edu/ReportCheckPOC/pocservlet?ReportNumber=";
		final String urlHCA = "http://www.pricescope.com/hca.php";
		final String urlReferer = "http://ideal-scope.com/online-holloway-cut-adviser/";
		
		File roundFile = new File("hca" + ".csv");
		
		final FileWriter w1 = new FileWriter(roundFile);
		
		w1.write(fullHeader);
		
		SAXParserFactory factory = SAXParserFactory.newInstance();
		SAXParser saxParser = factory.newSAXParser();

		DefaultHandler fullHandler = new DefaultHandler(){
			int count = 0;
			public void startElement(String uri, String localName, String qName, Attributes attributes)
				throws SAXException{
				count++;
				boolean success = false;
				if (!qName.equals("item"))
					return;
				String[] line = new String[40];
				for (int i = 0; i < attributes.getLength(); i++) {
					line[fullColIndex.get(attributes.getQName(i))] = attributes.getValue(i).replace(";", ",");
				}
				String textLine ="";
				for (int i = 0; i < 40; i++) {
					textLine += line[i] + DELI;
				}
				String hca = "", hcaRemark = "";
				boolean isMatched = true;
				for (String c : filterIndex.keySet()) {					
					if (filterIndex.get(c) == null) continue;
					List<String> options = Arrays.asList(filterIndex.get(c));
					if (line[fullColIndex.get(c)] == null || !options.contains(line[fullColIndex.get(c)].toLowerCase())){
						isMatched = false;
						break;
					}
				}
				if (isMatched){
					System.out.println("matched " + count);
					System.out.println(line[fullColIndex.get("cn")]);
					try {
						String url = urlGIA.replace("ReportNumber=", "ReportNumber=" + line[fullColIndex.get("cn")]);
						Document doc = Jsoup.connect(url).userAgent(agents[new Random().nextInt(agents.length)])
								.get();
						System.out.println("retrieved GIA info");
						Document doc2 = Jsoup.connect(urlHCA).referrer(urlReferer)
								.userAgent(agents[new Random().nextInt(agents.length)])
								.data("depth_textbox", doc.select("DEPTH_PCT").text())
								.data("table_textbox", doc.select("TABLE_PCT").text())
								.data("crown_listbox", "0")
								.data("crown_textbox", doc.select("CRN_AG").text())
								.data("pavilion_listbox", "0")
								.data("pavilion_textbox", doc.select("PAV_AG").text())
								.data("cutlet_textbox", doc.select("CULET_SIZE").text().replace("None", "0"))
								.post();
						System.out.println("retrieved HCA");
						Elements result = doc2.select("td:contains(Total Visual Performance):not(:has(td)) + td");
						if(result != null && !result.isEmpty()){
							System.out.println("retrieved from hca " + count);
							hcaRemark = result.get(0).text().trim();
							hca = hcaRemark.substring(0, hcaRemark.indexOf("-")).trim();
							success = true;
						}
					}catch (IOException e) {
						System.out.println(e.getMessage() + "-" + line[fullColIndex.get("id")]);
					}
					
					try{
						if (success)
							w1.write(textLine.replace("null", "") + date + ";" + hca + ";" + hcaRemark + "\n");
					}catch (Exception e){
						e.printStackTrace();
					}
				}
			}
			public void endElement(String uri, String localName, String qName) throws SAXException {				
			}

			public void characters(char ch[], int start, int length) throws SAXException {
			}
		};
				 
		{
		System.out.println("Retrieving HCA ratings...");
		if (args.length > 0){
			File newFile = new File(args[0]);    	
			saxParser.parse(newFile, fullHandler);
		}else{
			System.out.println("Processing full inventory...");
			File inputFile = new File("full.zip");
			String fullSource = "http://idexonline.com/Idex_Feed_API-Full_Inventory?String_Access=UVO4XXEMUCJLCRP7KF4JL9E12";
			FileUtils.copyURLToFile(new URL(fullSource), inputFile);
			ZipInputStream zis = new ZipInputStream(new FileInputStream(inputFile));
			ZipEntry ze = zis.getNextEntry();
			String fileName = ze.getName();
			File newFile = new File(fileName);
			FileOutputStream fos = new FileOutputStream(newFile);
			byte[] buffer = new byte[1024];
			int len;
	        while ((len = zis.read(buffer)) > 0) {
	        	fos.write(buffer, 0, len);
	        }
	        fos.close();
	        zis.closeEntry();
	    	zis.close();
			saxParser.parse(newFile, fullHandler);
		}
		w1.close();
		}
		System.out.println("Finished");
	}
}
