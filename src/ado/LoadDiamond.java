package ado;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.apache.commons.io.FileUtils;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class LoadDiamond {
	static Map<String, Integer> fullColIndex, updateColIndex;
	static String DELI = ";";
	static String fullHeader = "IDEX_Online_Item_ID;Supplier_Stock_Reference;Cut;Carat;Color;"
			+ "Natural_Fancy_Color;Natural_Fancy_Color_Intensity;Natural_Fancy_Color_Overtone;Treated_Color;Clarity;"
			+ "Make_Cut_Grade;Grading_Lab;Certificate_Number;Certificate_Path;Image_Path;"
			+ "Online_Report;Asking_Price_Per_Carat;Total_Price;Polish;Symmetry;"
			+ "Measurements;Total_Depth;Table_Width;Crown_Height;Pavilion_Depth;"
			+ "Girdle_From_To;Culet_Size;Culet_Condition;Graining;Fluorescence_Intensity;"
			+ "Fluorescence_Color;Enhancement;Supplier;Country;State_Region;"
			+ "Remarks;Phone;Matching_Pair_Stock_Reference;Email;Reference_to_IDEX_Price_Report;Date;HCA;HCA_Remark\n";
	static String updateHeader = "IDEX_Online_Item_ID;Status;Supplier_Stock_Reference;Cut;Carat;Color;"
			+ "Natural_Fancy_Color;Natural_Fancy_Color_Intensity;Natural_Fancy_Color_Overtone;Treated_Color;Clarity;"
			+ "Make_Cut_Grade;Grading_Lab;Certificate_Number;Certificate_Path;Image_Path;"
			+ "Online_Report;Asking_Price_Per_Carat;Total_Price;Polish;Symmetry;"
			+ "Measurements;Total_Depth;Table_Width;Crown_Height;Pavilion_Depth;"
			+ "Girdle_From_To;Culet_Size;Culet_Condition;Graining;Fluorescence_Intensity;"
			+ "Fluorescence_Color;Enhancement;Supplier;Country;State_Region;"
			+ "Remarks;Phone;Matching_Pair_Stock_Reference;Email;Reference_to_IDEX_Price_Report;Date;HCA;HCA_Remark\n";
	static void init(){
		fullColIndex = new HashMap<String, Integer>();
		fullColIndex.put("id", 0);
		fullColIndex.put("sr", 1);
		fullColIndex.put("cut", 2);
		fullColIndex.put("ct", 3);
		fullColIndex.put("col", 4);
		fullColIndex.put("nfc", 5);
		fullColIndex.put("nfci", 6);
		fullColIndex.put("nfco", 7);
		fullColIndex.put("tc", 8);
		fullColIndex.put("cl", 9);
		fullColIndex.put("mk", 10);
		fullColIndex.put("lab", 11);
		fullColIndex.put("cn", 12);
		fullColIndex.put("cp", 13);
		fullColIndex.put("ip", 14);
		fullColIndex.put("or", 15);
		fullColIndex.put("ap", 16);
		fullColIndex.put("tp", 17);
		fullColIndex.put("pol", 18);
		fullColIndex.put("sym", 19);
		fullColIndex.put("mes", 20);
		fullColIndex.put("dp", 21);
		fullColIndex.put("tb", 22);
		fullColIndex.put("cr", 23);
		fullColIndex.put("pv", 24);
		fullColIndex.put("gd", 25);
		fullColIndex.put("cs", 26);
		fullColIndex.put("cc", 27);
		fullColIndex.put("gr", 28);
		fullColIndex.put("fl", 29);
		fullColIndex.put("fc", 30);
		fullColIndex.put("en", 31);
		fullColIndex.put("sup", 32);
		fullColIndex.put("cty", 33);
		fullColIndex.put("st", 34);
		fullColIndex.put("rm", 35);
		fullColIndex.put("tel", 36);
		fullColIndex.put("psr", 37);
		fullColIndex.put("eml", 38);
		fullColIndex.put("idxl", 39);
		
		updateColIndex = new HashMap<String, Integer>();
		updateColIndex.put("id", 0);
		updateColIndex.put("status", 1);
		updateColIndex.put("sr", 2);
		updateColIndex.put("cut", 3);
		updateColIndex.put("ct", 4);
		updateColIndex.put("col", 5);
		updateColIndex.put("nfc", 6);
		updateColIndex.put("nfci", 7);
		updateColIndex.put("nfco", 8);
		updateColIndex.put("tc", 9);
		updateColIndex.put("cl", 10);
		updateColIndex.put("mk", 11);
		updateColIndex.put("lab", 12);
		updateColIndex.put("cn", 13);
		updateColIndex.put("cp", 14);
		updateColIndex.put("ip", 15);
		updateColIndex.put("or", 16);
		updateColIndex.put("ap", 17);
		updateColIndex.put("tp", 18);
		updateColIndex.put("pol", 19);
		updateColIndex.put("sym", 20);
		updateColIndex.put("mes", 21);
		updateColIndex.put("dp", 22);
		updateColIndex.put("tb", 23);
		updateColIndex.put("cr", 24);
		updateColIndex.put("pv", 25);
		updateColIndex.put("gd", 26);
		updateColIndex.put("cs", 27);
		updateColIndex.put("cc", 28);
		updateColIndex.put("gr", 29);
		updateColIndex.put("fl", 30);
		updateColIndex.put("fc", 31);
		updateColIndex.put("en", 32);
		updateColIndex.put("sup", 33);
		updateColIndex.put("cty", 34);
		updateColIndex.put("st", 35);
		updateColIndex.put("rm", 36);
		updateColIndex.put("tel", 37);
		updateColIndex.put("psr", 38);
		updateColIndex.put("eml", 39);
		updateColIndex.put("idxl", 40);
	}
	
	public static void main(String args[]) throws IOException, ParserConfigurationException, SAXException {
		init();
		Date time = Calendar.getInstance().getTime();
		final String date = new SimpleDateFormat("dd/MM/YYYY").format(time);
		
		File roundFile = new File("round" + ".csv");
		File othertFile = new File("other" + ".csv");
		File updateFile = new File("update" + ".csv");
		
		final FileWriter w0 = new FileWriter(updateFile);
		final FileWriter w1 = new FileWriter(roundFile);
		final FileWriter w2 = new FileWriter(othertFile);
		
		w0.write(updateHeader);
		w1.write(fullHeader);
		w2.write(fullHeader);
		
		SAXParserFactory factory = SAXParserFactory.newInstance();
		SAXParser saxParser = factory.newSAXParser();

		DefaultHandler updateHandler = new DefaultHandler(){
			int count = 0;
			public void startElement(String uri, String localName, String qName, Attributes attributes)
				throws SAXException{
				if (!qName.equals("item"))
					return;
				System.out.println(count++);
				String[] line = new String[41];
				for (int i = 0; i < attributes.getLength(); i++) {
					line[updateColIndex.get(attributes.getQName(i))] = attributes.getValue(i).replace(";", ",");
				}
				String textLine ="";
				for (int i = 0; i < 41; i++) {
					textLine += line[i] + DELI;
				}
				try{
						w0.write(textLine.replace("null", "") + date + ";;\n");
				}catch (Exception e){
//					e.printStackTrace();
				}
			}
		};

		DefaultHandler fullHandler = new DefaultHandler(){
			int count = 0;
			public void startElement(String uri, String localName, String qName, Attributes attributes)
				throws SAXException{
				if (!qName.equals("item"))
					return;
				System.out.println(count++);
				String[] line = new String[40];
				for (int i = 0; i < attributes.getLength(); i++) {
					line[fullColIndex.get(attributes.getQName(i))] = attributes.getValue(i).replace(";", ",");
				}
				String textLine ="";
				for (int i = 0; i < 40; i++) {
					textLine += line[i] + DELI;
				}
				String hca = "", hcaRemark = "";
				try{
					if (line[fullColIndex.get("cut")].equals("Round"))
						w1.write(textLine.replace("null", "") + date + ";" + hca + ";" + hcaRemark + "\n");
					else
						w2.write(textLine.replace("null", "") + date + ";" + hca + ";" + hcaRemark + "\n");
				}catch (Exception e){
//					e.printStackTrace();
				}
			}
			public void endElement(String uri, String localName, String qName) throws SAXException {				
			}

			public void characters(char ch[], int start, int length) throws SAXException {
			}
		};
				 
		{
		System.out.println("Processing updates...");
		File inputFile = new File("update.zip");
		String updateSource = "http://idexonline.com/Idex_Feed_API-Inventory_Update?String_Access=UVO4XXEMUCJLCRP7KF4JL9E12"; 
		FileUtils.copyURLToFile(new URL(updateSource), inputFile);
		ZipInputStream zis = new ZipInputStream(new FileInputStream(inputFile));
		ZipEntry ze = zis.getNextEntry();
		if (ze==null){
			System.out.println("No new updates!");
		}else{
			String fileName = ze.getName();
			File newFile = new File(fileName);
			FileOutputStream fos = new FileOutputStream(newFile);
			byte[] buffer = new byte[1024];
			int len;
	        while ((len = zis.read(buffer)) > 0) {
	        	fos.write(buffer, 0, len);
	        }
	        fos.close();
	        zis.closeEntry();
	    	zis.close();
	    	
			saxParser.parse(newFile, updateHandler);
		}
		w0.close();
		}		

		System.out.println("Processing full inventory...");
		File inputFile = new File("full.zip");
		String fullSource = "http://idexonline.com/Idex_Feed_API-Full_Inventory?String_Access=UVO4XXEMUCJLCRP7KF4JL9E12";
		FileUtils.copyURLToFile(new URL(fullSource), inputFile);
		ZipInputStream zis = new ZipInputStream(new FileInputStream(inputFile));
		ZipEntry ze = zis.getNextEntry();
		String fileName = ze.getName();
		File newFile = new File(fileName);
		FileOutputStream fos = new FileOutputStream(newFile);
		byte[] buffer = new byte[1024];
		int len;
        while ((len = zis.read(buffer)) > 0) {
        	fos.write(buffer, 0, len);
        }
        fos.close();
        zis.closeEntry();
    	zis.close();
    	
		saxParser.parse(newFile, fullHandler);
		w1.close();
		w2.close();

		System.out.println("Finished");
		HCA.main(new String[]{fileName});
	}
}
