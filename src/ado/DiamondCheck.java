package ado;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.jsoup.Jsoup;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class DiamondCheck {

	static String USER_AGENT = "Mozilla/5.0";

	@SuppressWarnings("resource")
	static String convertStreamToString(InputStream is) {
		Scanner s = new Scanner(is).useDelimiter("\\A");
		return s.hasNext() ? s.next() : "";
	}

	@SuppressWarnings("deprecation")
	public static void main(String args[]) throws ParserConfigurationException, SAXException, IOException {
		CloseableHttpClient client = HttpClients.createDefault();
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = factory.newDocumentBuilder();
		String urlGIA = "http://www.gia.edu/otmm_wcs_int/proxy-report/?url=https://myapps.gia.edu/ReportCheckPOC/pocservlet?ReportNumber=";
		String urlHRD = "https://my.hrdantwerp.com/?record_number=";
		String urlIGI = "http://www.igiworldwide.com/searchreport_postreq.php?r=";
		String urlHCA = "http://www.pricescope.com/hca.php";
		File inputFile = new File("res/Full.160315143129921.xml");

		{HttpPost request = new HttpPost(urlHCA);
		request.addHeader("Referer", "http://ideal-scope.com/online-holloway-cut-adviser/");
		List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
		urlParameters.add(new BasicNameValuePair("depth_textbox", "50"));
		urlParameters.add(new BasicNameValuePair("table_textbox", "57"));
		urlParameters.add(new BasicNameValuePair("crown_listbox", "0"));
		urlParameters.add(new BasicNameValuePair("crown_textbox", "36"));
		urlParameters.add(new BasicNameValuePair("pavilion_listbox", "0"));
		urlParameters.add(new BasicNameValuePair("pavilion_textbox", "42"));
		urlParameters.add(new BasicNameValuePair("cutlet_textbox", "0"));
		HttpEntity postParams = new UrlEncodedFormEntity(urlParameters);
		request.setEntity(postParams);
		System.out.println(request.toString());
		CloseableHttpResponse response = client.execute(request);
		System.out.println("Response Code : " + response.getStatusLine().getStatusCode());
		HttpEntity entity = response.getEntity();
		InputStream instream = entity.getContent();
		String res = convertStreamToString(instream);
		org.jsoup.nodes.Document doc = Jsoup.parse(res);
		for (org.jsoup.nodes.Element cell : doc.select("td:contains(Total Visual Performance)")) {
			if (cell.nextSibling() != null)
			System.out.println(cell.nextElementSibling().text());
		}
		System.out.println("----------------------------------------");
		
		}
				
		Document doc = dBuilder.parse(inputFile);
		doc.getDocumentElement().normalize();
		System.out.println("Root element :" + doc.getDocumentElement().getNodeName());

		NodeList nList = doc.getElementsByTagName("item");
		for (int i = 0; i < 10; i++) {
			Node nNode = nList.item(i);
			if (nNode.getNodeType() == Node.ELEMENT_NODE) {
				Element eElement = (Element) nNode;
				System.out.println(eElement.getAttribute("lab") + " : " + eElement.getAttribute("cn"));
				System.out.println(eElement.getAttribute("cr") + " : " + eElement.getAttribute("pv"));
				if (eElement.getAttribute("lab").equals("GIA")) {
					HttpGet request = new HttpGet(
							urlGIA.replace("ReportNumber=", "ReportNumber=" + eElement.getAttribute("cn")));
					HttpResponse response = client.execute(request);
					System.out.println("Response Code : " + response.getStatusLine().getStatusCode());
					HttpEntity entity = response.getEntity();
					InputStream instream = entity.getContent();
					String res = convertStreamToString(instream);
					{int begin = res.indexOf("<CRN_AG>") + 8;
					int end = res.indexOf("</CRN_AG>") - 1;
					if (end>begin){
						System.out.print(res.substring(begin,end));
					}}
					{int begin = res.indexOf("<PAV_AG>") + 8;
					int end = res.indexOf("</PAV_AG>") - 1;
					if (end>begin){
						System.out.println(res.substring(begin,end));
					}}
					if (entity != null)
						entity.consumeContent();
				}
				else if (eElement.getAttribute("lab").equals("HRD")) {
					HttpGet request = new HttpGet(
							urlHRD + eElement.getAttribute("cn"));
					HttpResponse response = client.execute(request);
					System.out.println("Response Code : " + response.getStatusLine().getStatusCode());
					HttpEntity entity = response.getEntity();
					InputStream instream = entity.getContent();
					String res = convertStreamToString(instream);
					{int begin = res.indexOf("<td>Crown Height (β):</td>") + 26;
					int end = begin + 28;
					if (end>begin){
						System.out.println(res.substring(begin,end));
					}}
					{int begin = res.indexOf("<td>Pavilion Depth (α):</td>") + 28;
					int end = begin + 28;
					if (end>begin){
						System.out.println(res.substring(begin,end));
					}}
					if (entity != null)
						entity.consumeContent();
				}
				else if (eElement.getAttribute("lab").equals("IGI")) {
					HttpGet request = new HttpGet(
							urlIGI + eElement.getAttribute("cn"));
					HttpResponse response = client.execute(request);
					System.out.println("Response Code : " + response.getStatusLine().getStatusCode());
					HttpEntity entity = response.getEntity();
					InputStream instream = entity.getContent();
					String res = convertStreamToString(instream).toLowerCase();
					{int begin = res.indexOf("crown height");
					int end = begin + 28;
					if (begin > -1 && end>begin){
						System.out.println(res.substring(begin,end));
					}}
					{int begin = res.indexOf("pavilion depth");
					int end = begin + 28;
					if (begin > -1 && end>begin){
						System.out.println(res.substring(begin,end));
					}}
					if (entity != null)
						entity.consumeContent();
				}

				System.out.println("----------------------------");

			}
		}
		client.close();
	}
}
